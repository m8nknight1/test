<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSensorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::create('sensor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 256)->nullable();
            $table->integer('id_user')->unsigned()->nullable();
            $table->integer('id_floor')->unsigned()->nullable();
            $table->index('id_user');
            $table->index('id_floor');
            $table->foreign('id_user')->
                references('id')->
                on('user')->
                onDelete('set null')->
                onUpdate('cascade');
            $table->foreign('id_floor')->
                references('id')->
                on('floor')->
                onDelete('set null')->
                onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sensor');
    }
}
