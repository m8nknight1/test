<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFloorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::create('floor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number')->nullable();
            $table->integer('id_building')->unsigned()->nullable();
            $table->index('id_building');
            $table->foreign('id_building')->
                references('id')->
                on('building')->
                onDelete('set null')->
                onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('floor');
    }
}
