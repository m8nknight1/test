<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserRolesTableSeeder::class);
        $this->command->info('Таблица ролей загружена данными');

        $this->call(UserTableSeeder::class);
        $this->command->info('Таблица пользователей загружена данными');

        $this->call(BuildingTableSeeder::class);
        $this->command->info('Таблица зданий загружена данными');

        $this->call(FloorTableSeeder::class);
        $this->command->info('Таблица этажей загружена данными');

        $this->call(sensorTableSeeder::class);
        $this->command->info('Таблица датчиков загружена данными');
    }
}

class UserRolesTableSeeder extends Seeder{
    public function run(){
        DB::delete('delete from user_role');
        DB::table('user_role')->insert(['role' => 'admin', 'id' => 1]);
        DB::table('user_role')->insert(['role' => 'user', 'id' => 2]);
    }
}

class UserTableSeeder extends Seeder{
    public function run(){
        DB::delete('delete from user');
        DB::table('user')->insert(['name' => 'Админ', 'id' => 1, 'id_role' => 1, 'email' => 'admin@mail.com', 'password' => bcrypt(111111)]);
        DB::table('user')->insert(['name' => 'Пользователь', 'id' => 2, 'id_role' => 2, 'email' => 'user@mail.com', 'password' => bcrypt(111111)]);
    }
}

class BuildingTableSeeder extends Seeder{
    public function run(){
        DB::delete('delete from building');
        DB::table('building')->insert(['address' => 'ул. Мира дом 5', 'id' => 1]);
        DB::table('building')->insert(['address' => 'ул. Сумская дом 25', 'id' => 2]);
    }
}

class FloorTableSeeder extends Seeder{
    public function run(){
        DB::delete('delete from floor');
        DB::table('floor')->insert(['id' => 1, 'number' => 1, 'id_building' => 1]);
        DB::table('floor')->insert(['id' => 2, 'number' => 2, 'id_building' => 1]);
        DB::table('floor')->insert(['id' => 3, 'number' => 3, 'id_building' => 1]);
        DB::table('floor')->insert(['id' => 4, 'number' => 4, 'id_building' => 1]);
        DB::table('floor')->insert(['id' => 5, 'number' => 5, 'id_building' => 1]);
        DB::table('floor')->insert(['id' => 6, 'number' => 6, 'id_building' => 1]);
        DB::table('floor')->insert(['id' => 7, 'number' => 7, 'id_building' => 1]);
        
        DB::table('floor')->insert(['id' => 8, 'number' => 1, 'id_building' => 2]);
        DB::table('floor')->insert(['id' => 9, 'number' => 2, 'id_building' => 2]);
        DB::table('floor')->insert(['id' => 10, 'number' => 3, 'id_building' => 2]);
        DB::table('floor')->insert(['id' => 11, 'number' => 4, 'id_building' => 2]);
        DB::table('floor')->insert(['id' => 12, 'number' => 5, 'id_building' => 2]);
        DB::table('floor')->insert(['id' => 13, 'number' => 6, 'id_building' => 2]);
        DB::table('floor')->insert(['id' => 14, 'number' => 7, 'id_building' => 2]);
    }
}

class SensorTableSeeder extends Seeder{
    public function run(){
        DB::delete('delete from sensor');
        DB::table('sensor')->insert(['id' => 1, 'id_floor' => 1, 'name' => 'Датчик дождя', 'id_user' => 2]);
        DB::table('sensor')->insert(['id' => 2, 'id_floor' => 2, 'name' => 'Датчик света', 'id_user' => 2]);
        DB::table('sensor')->insert(['id' => 3, 'id_floor' => 3, 'name' => 'Датчик газа', 'id_user' => 2]);
        DB::table('sensor')->insert(['id' => 4, 'id_floor' => 4, 'name' => 'Датчик огня', 'id_user' => 1]);
        DB::table('sensor')->insert(['id' => 5, 'id_floor' => 5, 'name' => 'Датчик огня', 'id_user' => 2]);
        DB::table('sensor')->insert(['id' => 6, 'id_floor' => 6, 'name' => 'Датчик газа', 'id_user' => 1]);
        DB::table('sensor')->insert(['id' => 7, 'id_floor' => 7, 'name' => 'Датчик огня', 'id_user' => 2]);
        
        DB::table('sensor')->insert(['id' => 8, 'id_floor' => 1, 'name' => 'Датчик света', 'id_user' => 1]);
        DB::table('sensor')->insert(['id' => 9, 'id_floor' => 2, 'name' => 'Датчик света', 'id_user' => 2]);
        DB::table('sensor')->insert(['id' => 10, 'id_floor' => 3, 'name' => 'Датчик огня', 'id_user' => 1]);
        DB::table('sensor')->insert(['id' => 11, 'id_floor' => 4, 'name' => 'Датчик газа', 'id_user' => 2]);
        DB::table('sensor')->insert(['id' => 12, 'id_floor' => 5, 'name' => 'Датчик газа', 'id_user' => 1]);
        DB::table('sensor')->insert(['id' => 13, 'id_floor' => 6, 'name' => 'Датчик тепла', 'id_user' => 2]);
        DB::table('sensor')->insert(['id' => 14, 'id_floor' => 7, 'name' => 'Датчик движения', 'id_user' => 2]);
    }
}
