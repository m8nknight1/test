@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="padding: 10px;">                
                Этажи зданий
                @include('links')
            </div>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Ошибка</strong>
                <br>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif
            <div class="row">
                @if (Auth::user()->getRole() == 'admin')
                <div class="col-md-12">
                    <form action="{{ url('floors') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="task" class="col-sm-4 control-label">Номер этажа</label>

                            <div class="col-sm-8">
                                <input type="text" name="number" id="floor-number" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="task" class="col-sm-4 control-label">Здание</label>

                            <div class="col-sm-8">
                                <select prompt="- Выберите здание -" name="id_building" id="floor-id_building" class="form-control">
                                @foreach ($buildings as $building)
                                    <option {{ $active_building->id == $building->id ? 'selected' : '' }} value="{{ $building->id }}">{{ $building->address }}</option>
                                @endforeach
                                <select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-plus"></i> Добавить этаж
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                @endif
                <div class="col-md-12">
                @if (count($floors) > 0)
                    <table style="width: 100%;">
                        <tr>
                            <th>Номер этажа</th>
                            <th>Адрес здания</th>
                            <th>Количество датчиов</th>
                            <th class="text-center">|||</th>
                        </tr>
                        @foreach ($floors as $floor)
                            <tr>
                                <td style="padding: 10px;">{{ $floor->number }}</td>
                                <td style="padding: 10px;">{{ $floor->building->address }}</td>
                                <td class="text-center"><a href="{{ url('sensors/'.$floor->id) }}">{{ count($floor->sensors) }}</a></td>
                                <td style="padding: 10px;">
                                    @if (Auth::user()->getRole() == 'admin')
                                    <form action="{{ url('floors/'.$floor->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}

                                        <button type="submit" class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash"></i> Удалить
                                        </button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @endif
                </div>                
            </div>
        </div>
    </div>
</div>
@endsection