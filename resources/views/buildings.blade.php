@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="padding: 10px;">                
                Здания
                @include('links')
            </div>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Ошибка</strong>
                <br>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif
            <div class="row">
                <div class="col-md-6">
                @if (count($buildings) > 0)
                    <table style="width: 100%;">
                        <tr>
                            <th>Адрес</th>
                            <th>Этажей</th>
                            <th class="text-center">|||</th>
                        </tr>
                        @foreach ($buildings as $building)
                            <tr>
                                <td style="padding: 10px;">{{$building->address}}</td>
                                <td class="text-center"><a href="{{ url('floors/'.$building->id) }}">{{count($building->floors)}}</a></td>
                                <td style="padding: 10px;">
                                    @if (Auth::user()->getRole() == 'admin')
                                    <form action="{{ url('buildings/'.$building->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}

                                        <button type="submit" class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash"></i> Удалить
                                        </button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @endif
                </div>
                <div class="col-md-6">
                    @if (Auth::user()->getRole() == 'admin')
                    <form action="{{ url('buildings') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="task" class="col-sm-4 control-label">Адрес</label>

                            <div class="col-sm-8">
                                <input type="text" name="address" id="bilding-address" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-plus"></i> Добавить здание
                                </button>
                            </div>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection