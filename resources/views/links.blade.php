<div class="row">
    <div class="col-md-4 text-center"><a href="/buildings" class="btn btn-primary btn-sm">Здания</a></div>
    <div class="col-md-4 text-center"><a href="/floors" class="btn btn-primary btn-sm">Этажи</a></div>
    <div class="col-md-4 text-center"><a href="/sensors" class="btn btn-primary btn-sm">Датчики</a></div>
</div>