@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="padding: 10px;">                
                Сенсоры на этажах
                @include('links')
            </div>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Ошибка</strong>
                <br>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ url('sensors') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="task" class="col-sm-4 control-label">Название датчика</label>

                            <div class="col-sm-8">
                                <input type="text" name="name" id="sensor-name" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="task" class="col-sm-4 control-label">Этаж</label>

                            <div class="col-sm-8">
                                <select prompt="- Выберите здание -" name="id_floor" id="sensor-id_floor" class="form-control">
                                @foreach ($floors as $floor)
                                    <option {{ $active_floor->id == $floor->id ? 'selected' : '' }} value="{{ $floor->id }}">{{ $floor->number }} этаж на {{$floor->building->address}}</option>
                                @endforeach
                                <select>
                            </div>
                        </div>
                        @if (Auth::user()->getRole() == 'admin')
                        <div class="form-group">
                            <label for="task" class="col-sm-4 control-label">Пользователь</label>

                            <div class="col-sm-8">
                                <select prompt="- Выберите здание -" name="id_user" id="sensor-id_user" class="form-control">
                                @foreach ($users as $user)
                                    <option {{ Auth::user()->id == $user->id ? 'selected' : '' }} value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                                <select>
                            </div>
                        </div>
                        @endif
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-plus"></i> Добавить датчик
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-12">
                @if (count($sensors) > 0)
                    <table style="width: 100%;">
                        <tr>
                            <th>Номер этажа</th>
                            <th>Адрес здания</th>
                            <th>Название датчика</th>
                            <th>Добавлен пользователем</th>
                            <th class="text-center">|||</th>
                        </tr>
                        @foreach ($sensors as $sensor)
                            <tr style="padding: 10px;">
                                <td>{{ $sensor->floor->number }}</td>
                                <td>{{ $sensor->floor->building->address }}</td>
                                <td>{{ $sensor->name }}</td>
                                <td>{{ $sensor->user->name }}</td>
                                <td style="padding: 10px;">
                                    @if (Auth::user()->getRole() == 'admin' || Auth::user()->id == $sensor->user->id)
                                    <form action="{{ url('sensors/'.$sensor->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}

                                        <button type="submit" class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash"></i> Удалить
                                        </button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @endif
                </div>                
            </div>
        </div>
    </div>
</div>
@endsection