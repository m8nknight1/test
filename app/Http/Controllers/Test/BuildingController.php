<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\PDO\User;
use App\Http\PDO\Building;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query\Builder;

class BuildingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        return view('buildings', ['buildings' => Building::all()]);
    }

    public function delete(Building $building){
        if(Auth::user()->getRole() != 'admin'){
            return '403';
        }
        $building->delete();
        return redirect('/buildings');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'address' => 'required|string|max:255',            
        ]);
    }

    public function add(Request $request){
        if(Auth::user()->getRole() != 'admin'){
            return '403';
        }
                
        if ($validator = $this->validator($request->all())->fails()) {
            return redirect('/buildings')
              ->withInput()
              ->withErrors($validator);
        }

        $building = new Building();
        $building->address = $request->address;
        $building->save();

        return redirect('/buildings');
    }
}