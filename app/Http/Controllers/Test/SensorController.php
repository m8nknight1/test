<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\PDO\User;
use App\Http\PDO\Building;
use App\Http\PDO\Floor;
use App\Http\PDO\Sensor;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query\Builder;

class SensorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Floor $floor)
    {   
        $sensors = $floor->id ? $floor->sensors : Sensor::all();     
        return view('sensors', ['sensors' => $sensors, 'active_floor' => $floor, 'users' => User::all(), 'floors' => Floor::all()]);
    }

    public function delete(Sensor $sensor){
        if(Auth::user()->getRole() == 'admin' || Auth::user()->id == $sensor->user->id){
            $sensor->delete();
            return redirect('/sensors');
        }
        else{
            return '403';
        }        
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'string|required|max:255',
            'id_floor' => 'integer|required',           
        ]);
    }

    public function add(Request $request){        
        
        if ($validator = $this->validator($request->all())->fails()) {
            return redirect('/sensors')
              ->withInput()
              ->withErrors($validator);
        }

        $sensor = new Sensor();
        $sensor->name = $request->name;
        $sensor->id_floor = $request->id_floor;
        if(Auth::user()->getRole() == 'admin'){
            $sensor->id_user = $request->id_user;
        }
        else{
            $sensor->id_user = Auth::user()->id;
        }
        
        $sensor->save();

        return redirect('/sensors/'.$request->id_floor);
    }
}