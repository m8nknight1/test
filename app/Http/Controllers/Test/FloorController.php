<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\PDO\User;
use App\Http\PDO\Building;
use App\Http\PDO\Floor;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query\Builder;

class FloorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Building $building)
    {   
        $floors = $building->id ? $building->floors : Floor::all();     
        return view('floors', ['buildings' => Building::all(), 'floors' => $floors, 'active_building' => $building]);
    }

    public function delete(Floor $floor){
        if(Auth::user()->getRole() != 'admin'){
            return '403';
        }
        $floor->delete();
        return redirect('/floors');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'number' => 'integer|required',
            'id_building' => 'integer|required'            
        ]);
    }

    public function add(Request $request){
        if(Auth::user()->getRole() != 'admin'){
            return '403';
        }
        
        if ($validator = $this->validator($request->all())->fails()) {
            return redirect('/floors')
              ->withInput()
              ->withErrors($validator);
        }

        $floor = new Floor();
        $floor->number = $request->number;
        $floor->id_building = $request->id_building;
        $floor->save();

        return redirect('/floors/'.$request->id_building);
    }
}