<?php

namespace App\Http\PDO;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $table = 'building';
    public $timestamps = false;

    public function floors(){
        return $this->hasMany('App\Http\PDO\Floor', 'id_building', 'id');
    }
}
