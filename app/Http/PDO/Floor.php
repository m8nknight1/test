<?php

namespace App\Http\PDO;

use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    protected $table = 'floor';
    public $timestamps = false;

    public function building(){
        return $this->belongsTo('App\Http\PDO\Building', 'id_building', 'id');
    }

    public function sensors(){
        return $this->hasMany('App\Http\PDO\Sensor', 'id_floor', 'id');
    }
}
