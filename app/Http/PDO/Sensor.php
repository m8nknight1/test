<?php

namespace App\Http\PDO;

use Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{
    protected $table = 'sensor';
    public $timestamps = false;

    public function floor(){
        return $this->belongsTo('App\Http\PDO\Floor', 'id_floor', 'id');
    }

    public function user(){
        return $this->belongsTo('App\Http\PDO\User', 'id_user', 'id');
    }
}
