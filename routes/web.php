<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/buildings', 'Test\BuildingController@index')->name('Список зданий');
Route::post('/buildings', 'Test\BuildingController@add');
Route::delete('/buildings/{building}', 'Test\BuildingController@delete');

Route::get('/floors/{building}', 'Test\FloorController@index')->name('Список этажей');
Route::get('/floors', 'Test\FloorController@index')->name('Список этажей');
Route::post('/floors', 'Test\FloorController@add');
Route::delete('/floors/{floor}', 'Test\FloorController@delete');

Route::get('/sensors/{floor}', 'Test\SensorController@index')->name('Список сенсоров');
Route::get('/sensors', 'Test\SensorController@index')->name('Список сенсоров');
Route::post('/sensors', 'Test\SensorController@add');
Route::delete('/sensors/{sensor}', 'Test\SensorController@delete');